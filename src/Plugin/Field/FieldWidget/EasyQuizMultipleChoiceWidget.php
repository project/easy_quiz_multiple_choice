<?php

/**
 * @file
 * Contains \Drupal\easy_quiz_multiple_choice\Plugin\Field\FieldWidget\EasyQuizMultipleChoiceWidget.
 */

namespace Drupal\easy_quiz_multiple_choice\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\WidgetInterface;

/**
 * Plugin implementation of the 'answer_options' widget.
 *
 * @FieldWidget (
 *   id = "easy_quiz_multiple_choice",
 *   label = @Translation("Easy Quiz Multiple Choice widget"),
 *   field_types = {
 *     "easy_quiz_multiple_choice"
 *   }
 * )
 */
class EasyQuizMultipleChoiceWidget extends WidgetBase {
  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element['description'] = array(
      '#type' => 'textfield',
      '#title' => t('Description of the answer option'),
      '#default_value' => isset($items[$delta]->description) ? $items[$delta]->description : '',
      '#size' => 30,
      '#maxlength' => 500,
    );
    $element['state'] = array(
      '#type' => 'checkbox',
      '#title' => t('Correct answer'),
      '#default_value' => isset($items[$delta]->state) ? $items[$delta]->state : '',
    );


    // If cardinality is 1, ensure a label is output for the field by wrapping
    // it in a details element.
    if ($this->fieldDefinition->getFieldStorageDefinition()->getCardinality() == 1) {
      $element += array(
        '#type' => 'fieldset',
        '#attributes' => array('class' => array('container-inline')),
      );
    }

    return $element;
  }
}