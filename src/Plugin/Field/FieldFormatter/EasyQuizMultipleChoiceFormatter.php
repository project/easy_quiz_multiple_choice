<?php

/**
 * @file
 * Contains \Drupal\easy_quiz_multiple_choice\Plugin\Field\FieldFormatter\EasyQuizMultipleChoiceFormatter.
 */

namespace Drupal\easy_quiz_multiple_choice\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'answers' formatter.
 *
 * @FieldFormatter (
 *   id = "easy_quiz_multiple_choice",
 *   label = @Translation("EasyQuizMultipleChoice"),
 *   field_types = {
 *     "easy_quiz_multiple_choice"
 *   }
 * )
 */
class EasyQuizMultipleChoiceFormatter extends FormatterBase {
  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode = NULL) {
    $elements = array();

    foreach ($items as $delta => $item) {

      $markup = $item->description. ' - '. $item->state;

      $elements[$delta] = array(
        '#type' => 'markup',
        '#markup' => $markup,
      );
    }

    return $elements;
  }
}