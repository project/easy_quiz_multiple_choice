<?php

/**
 * @file
 * Contains \Drupal\easy_quiz_multiple_choice\Plugin\Field\FieldType\EasyQuizMultipleChoice.
 */

namespace Drupal\easy_quiz_multiple_choice\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'Answer Options' field type.
 *
 * @FieldType (
 *   id = "easy_quiz_multiple_choice",
 *   label = @Translation("Easy Quiz Multiple Choice"),
 *   description = @Translation("Multiple choice answer options for easy quiz"),
 *   default_widget = "easy_quiz_multiple_choice",
 *   default_formatter = "easy_quiz_multiple_choice",
 * )
 */
class EasyQuizMultipleChoice extends FieldItemBase {
  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return array(
      'columns' => array(
        'description' => array(
          'type' => 'text',
        ),
        'state' => array(
          'type' => 'text',
        ),
      ),
    );
  }

  /**
   * {@inheritdoc}
  */
  public function isEmpty() {
    $value1 = $this->get('description')->getValue();
    $value2 = $this->get('state')->getValue();
    return empty($value1) && empty($value2) && empty($value3);
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    // Add our properties.
    $properties['description'] = DataDefinition::create('string')
      ->setLabel(t('Description'))
      ->setDescription(t('Description of the answer option'))
      ->setRequired(TRUE);;

    $properties['state'] = DataDefinition::create('boolean')
      ->setLabel(t('State'));
    return $properties;
  }
}